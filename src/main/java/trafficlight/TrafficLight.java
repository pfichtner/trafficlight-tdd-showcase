package trafficlight;

public interface TrafficLight {

	public enum State {
		GO, STOP
	}

	void setState(State state);

	State getState();

}

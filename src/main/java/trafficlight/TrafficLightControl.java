package trafficlight;

import static trafficlight.TrafficLight.State.STOP;

import java.util.Arrays;
import java.util.List;

import trafficlight.TrafficLight.State;

public class TrafficLightControl {

	private List<TrafficLight> trafficLights;

	public void register(TrafficLight... trafficLights) {
		this.trafficLights = Arrays.asList(trafficLights.clone());
	}

	public void switchState(TrafficLight trafficLight, State state) {
		if (state == State.GO) {
			for (TrafficLight other : trafficLights) {
				if (other != trafficLight) {
					other.setState(STOP);
				}
			}
		}
		trafficLight.setState(state);
	}

}

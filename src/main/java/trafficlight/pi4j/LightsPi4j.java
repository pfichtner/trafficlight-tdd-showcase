package trafficlight.pi4j;

import static com.pi4j.io.gpio.PinState.HIGH;
import static com.pi4j.io.gpio.PinState.LOW;
import static com.pi4j.io.gpio.RaspiPin.GPIO_01;
import static com.pi4j.io.gpio.RaspiPin.GPIO_02;
import static com.pi4j.io.gpio.RaspiPin.GPIO_03;
import static trafficlight.Light.GREEN;
import static trafficlight.Light.RED;
import static trafficlight.Light.YELLOW;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import trafficlight.Light;
import trafficlight.Lights;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;

public class LightsPi4j implements Lights {

	private final GpioController gpio = GpioFactory.getInstance();
	private final Map<Light, GpioPinDigitalOutput> pins = new EnumMap<Light, GpioPinDigitalOutput>(
			Light.class);

	public LightsPi4j() {
		pins.put(RED, newPin(GPIO_01));
		pins.put(YELLOW, newPin(GPIO_02));
		pins.put(GREEN, newPin(GPIO_03));
		for (GpioPinDigitalOutput pin : pins.values()) {
			pin.setShutdownOptions(true, LOW);
		}
	}

	private GpioPinDigitalOutput newPin(Pin pin) {
		return gpio.provisionDigitalOutputPin(pin);
	}

	public void lightsOn(Light... lightsArray) {
		List<Light> lights = Arrays.asList(lightsArray);
		for (Entry<Light, GpioPinDigitalOutput> entry : pins.entrySet()) {
			PinState state = lights.contains(entry.getKey()) ? HIGH : LOW;
			setStateOfPin(entry.getValue(), state);
		}
	}

	private void setStateOfPin(GpioPinDigitalOutput pin, PinState pinState) {
		if (pin.getState() != pinState) {
			pin.setState(pinState);
		}
	}

}

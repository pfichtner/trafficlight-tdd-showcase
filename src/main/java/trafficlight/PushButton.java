package trafficlight;

import trafficlight.TrafficLight.State;

public class PushButton implements Sensor {

	public static class Builder {

		private final TrafficLight trafficLight;
		private State state;

		public Builder(TrafficLight trafficLight) {
			this.trafficLight = trafficLight;
		}

		public Builder thatSwitchesTo(State state) {
			this.state = state;
			return this;
		}

		public PushButton using(TrafficLightControl control) {
			return new PushButton(state, trafficLight, control);
		}

	}

	private final State state;
	private final TrafficLight trafficLight;
	private TrafficLightControl control;

	public PushButton(State state, TrafficLight trafficLight,
			TrafficLightControl control) {
		this.state = state;
		this.trafficLight = trafficLight;
		this.control = control;
	}

	public static Builder buttonFor(TrafficLight trafficLight) {
		return new Builder(trafficLight);

	}

	public void press() {
		control.switchState(trafficLight, state);
	}

}

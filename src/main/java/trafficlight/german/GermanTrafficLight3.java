package trafficlight.german;

import trafficlight.Light;
import trafficlight.Lights;
import trafficlight.TrafficLight;

public class GermanTrafficLight3 implements TrafficLight {

	private Lights lights;

	private State state = State.STOP;

	public GermanTrafficLight3(Lights lights) {
		this.lights = lights;
		lights.lightsOn(Light.RED);
	}

	public void setState(State state) {
		if (stopToGo(state)) {
			lights.lightsOn(Light.RED, Light.YELLOW);
			lights.lightsOn(Light.GREEN);
			this.state = state;
		} else if (goToStop(state)) {
			lights.lightsOn(Light.YELLOW);
			lights.lightsOn(Light.RED);
			this.state = state;
			this.state = state;
		}
	}

	private boolean goToStop(State state) {
		return this.state == State.GO && state == State.STOP;
	}

	private boolean stopToGo(State state) {
		return this.state == State.STOP && state == State.GO;
	}

	public State getState() {
		return state;
	}

}

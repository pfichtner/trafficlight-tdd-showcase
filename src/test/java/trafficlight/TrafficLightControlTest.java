package trafficlight;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static trafficlight.PushButton.buttonFor;
import static trafficlight.TrafficLight.State.GO;
import static trafficlight.TrafficLight.State.STOP;

import org.junit.Test;

import trafficlight.TrafficLight.State;

public class TrafficLightControlTest {

	private static class TrafficLightForTest implements TrafficLight {

		private State state;

		public void setState(State state) {
			this.state = state;
		}

		public State getState() {
			return state;
		}

	}

	private TrafficLightControl sut = new TrafficLightControl();

	@Test
	public void switchesSingleTrafficLightFromGoToStop() {
		TrafficLight aLight = trafficLightWithState(GO);
		buttonSwitchingTo(aLight, STOP).press();
		assertThat(aLight.getState(), is(STOP));
	}

	@Test
	public void switchesSecondToStopWhenOtherGoesToGo() {
		TrafficLight aLight = trafficLightWithState(GO);
		TrafficLight anotherLight = trafficLightWithState(STOP);
		sut.register(aLight, anotherLight);
		buttonSwitchingTo(anotherLight, GO).press();
		assertThat(aLight.getState(), is(STOP));
	}

	private TrafficLight trafficLightWithState(State state) {
		TrafficLight trafficLight = new TrafficLightForTest();
		trafficLight.setState(state);
		return trafficLight;
	}

	private PushButton buttonSwitchingTo(TrafficLight aLight, State state) {
		return buttonFor(aLight).thatSwitchesTo(state).using(sut);
	}
}

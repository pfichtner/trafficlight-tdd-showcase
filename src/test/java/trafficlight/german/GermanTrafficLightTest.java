package trafficlight.german;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static trafficlight.Light.GREEN;
import static trafficlight.Light.RED;
import static trafficlight.Light.YELLOW;
import static trafficlight.TrafficLight.State.GO;
import static trafficlight.TrafficLight.State.STOP;

import org.junit.Test;
import org.mockito.InOrder;

import trafficlight.Lights;
import trafficlight.TrafficLight;

public class GermanTrafficLightTest {

	private final Lights lights = mock(Lights.class);
	private final TrafficLight sut = new GermanTrafficLight3(lights);

	@Test
	public void defaultStateIsStop() {
		verify(lights).lightsOn(RED);
		verifyNoMoreInteractions(lights);
		assertThat(sut.getState(), is(STOP));
	}

	@Test
	public void switchFromRedToGreen() {
		reset(lights);
		sut.setState(GO);
	    InOrder orderVerifier = inOrder(lights);
	    orderVerifier.verify(lights).lightsOn(RED, YELLOW);
	    orderVerifier.verify(lights).lightsOn(GREEN);
		verifyNoMoreInteractions(lights);
		assertThat(sut.getState(), is(GO));
	}

	@Test
	public void switchFromGreenToRed() {
		sut.setState(GO);
		reset(lights);
		sut.setState(STOP);
	    InOrder orderVerifier = inOrder(lights);
	    orderVerifier.verify(lights).lightsOn(YELLOW);
	    orderVerifier.verify(lights).lightsOn(RED);
		verifyNoMoreInteractions(lights);
		assertThat(sut.getState(), is(STOP));
	}

}

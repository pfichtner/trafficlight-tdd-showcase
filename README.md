# trafficlight-tdd-showcase [![pipeline status](https://gitlab.com/pfichtner/trafficlight-tdd-showcase/badges/master/pipeline.svg)](https://gitlab.com/pfichtner/trafficlight-tdd-showcase/commits/master)

What's that showcase for? A fellow and me had a discussion about a test driven way to implement a traffic light control using the GPIO pins of an raspberry pi. 
We asked ourself if the controller has to check for the right sequence of the red-yellow-green phases.  
I then did implement using TDD (test driven development) with an inside-out (london school TDD) approach. As you can see, the controller does not care for any light (nor a red, yellow or green one). It just wants to switch traffic lights from "go" to "stop" and vice versa no matter how the traffic light implementations will handle that. 
On the other hand there is an implementation of a German three light traffic light (as we would like to build in hardware using the GPIOs). Of course the test for *that* implementation will check the sequence of the lights when going from red-to-green and green-to-red. To do that, a component that controls the lights was introduces as an interface so that we can verify the correct behavior in our tests. The only thing to test manually is to verify that the GPIO-controlling-interface-implementation really switches the red light when it's told to and the same for yellow and green. 

So you can add/refactor/remove code as you like to and most of the code can be verified to correct right without the use of any hardware (and without an manual testing). 
